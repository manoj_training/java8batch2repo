import java.util.function.BiConsumer;

public class BiConsumerExample {

	public static void main(String[] args) {
		
		
		BiConsumer<String,Integer> biCon=(s,n)->{
			for(int i=0; i<n; i++) {
				System.out.println(s.charAt(i));
			}
		};
		
		biCon.accept("hello", 3);

	}

}
