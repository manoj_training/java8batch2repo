package functional;

import java.util.function.BiPredicate;

public class BiPredicateExample {

	public static boolean checkAge(Person p1, Person p2, BiPredicate<Person,Person> bipredicate) {
		return bipredicate.test(p1, p2);
	}
	
	public static void main(String[] args) {
	
		Person p1=new Person("AA",25); Person p2=new Person("CD",28);
		
		boolean res=checkAge(p1,p2,(firstPerson,secondPerson)->firstPerson.getAge()>secondPerson.getAge());
		System.out.println(res);
	}

}
