package functional;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerExample {

	public static void operate(Number number, Consumer<Number> consumer) 
	{
		consumer.accept(number);
	}

	public static void changePersonName(Person person, Consumer<Person> consumer) {
		consumer.accept(person);
	}
	
	public static void main(String[] args) {

		operate(10,(num)->System.out.println(num));
		Person p1=new Person("ashish",25);
		Person p2=new Person("ravi",35);
		Person p3=new Person("sam",45);
		Person p4=new Person("john",55);
		System.out.println(p1);
		changePersonName(p1,(person)->{
			person.setName(person.getName().toUpperCase());			
		});
		System.out.println(p1);
		
		
		
		List<Person> persons=Arrays.asList(p1,p2,p3,p4);
		
		System.out.println(persons);
		
		//persons.forEach((person)->{person.setAge(person.getAge()+1);});
		persons.forEach(new PersonService()::changeAge);
		/*
		for(Person person:persons) {
			person.setAge(person.getAge()+1);
		}
		*/
		System.out.println(persons);
		
		
		
		
		
		
		
		
		
		
		
	}

}
