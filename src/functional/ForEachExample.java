package functional;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ForEachExample {

	public static void main(String[] args) {
	
		List<String> names=Arrays.asList("amit","rajeev","riya","priya");
		
		Consumer<String> consumer=System.out::println;
		
		names.forEach(consumer);
		
		
		
	}

}
