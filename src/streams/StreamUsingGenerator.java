package streams;

import java.util.Random;
import java.util.stream.Stream;

public class StreamUsingGenerator {

	public static void main(String[] args) {
		Stream<Integer> stream=Stream.generate(()->(new Random()).nextInt(100)).limit(10);
		stream.forEach(System.out::println);
		
		stream.forEach(System.out::println);

	}

}
