package streams;

import java.util.HashSet;
import java.util.Random;
import java.util.stream.Stream;

public class StreamUsingCollection {

	public static void main(String[] args) {
		
		HashSet<String> set=new HashSet<>(); 
		set.add("java"); set.add("sql"); set.add("plsql"); set.add("python");
		set.stream().forEach(System.out::println);

		
		
	}

}
