package streams;

import java.util.ArrayList;
import java.util.List;

public class MatchingStream {
	public static void main(String[] args) {
		List<Employee> list=new ArrayList<>();
		list.add(new Employee(5001,"Rajesh",76000));
		list.add(new Employee(3001,"James",65000));
		list.add(new Employee(1001,"Akash",84000));
		list.add(new Employee(2001,"John",50000));
		list.add(new Employee(4001,"Michele",32000));
		
	
		//boolean found=list.stream().anyMatch((employee)->employee.getSalary()>=75000);
		//boolean found=list.stream().allMatch((employee)->employee.getSalary()>=50000);
		boolean found=list.stream().noneMatch((employee)->employee.getSalary()<=20000);
		System.out.println(found);
		
	}
}
