package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SortingStream {

	public static void main(String[] args) {
		
		List<Employee> list=new ArrayList<>();
		list.add(new Employee(5001,"Rajesh",76000));
		list.add(new Employee(3001,"James",65000));
		list.add(new Employee(1001,"Akash",84000));
		list.add(new Employee(2001,"John",50000));
		list.add(new Employee(4001,"Michele",92000));
		
		List<Employee> sortedEmployees=list.stream().sorted((e1,e2)->e1.getSalary()-e2.getSalary()).collect(Collectors.toList());
		System.out.println("___________________________________________");
		for(Employee emp:sortedEmployees) {
			System.out.println(emp);
		}
		
	}

}
