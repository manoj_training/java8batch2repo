package streams;

import java.util.stream.IntStream;

public class StreamUsingString {

	public static void main(String[] args) {
		String s="INDIA";
		IntStream stream=s.chars();
		stream.forEach((num)->{System.out.println((char)num);});
	}

}
