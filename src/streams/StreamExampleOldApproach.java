package streams;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;



public class StreamExampleOldApproach {
public static void main(String[] args) {
		
		Stream<Integer> stream1=Stream.of(10,20,30,40,50,60,70);
		Stream<Integer> stream2=stream1.filter(new MyPredicate());
		stream2.forEach(new MyConsumer());
		System.out.println("End-of-Main");
		

	}

	static class MyConsumer implements Consumer<Integer> {
		@Override
		public void accept(Integer n) {
			System.out.println(n);
		}
	}
	static class MyPredicate implements Predicate<Integer> {
		@Override
		public boolean test(Integer n) {
			return n%20==0;
		}
		
	}
}
