package streams;

import java.util.Optional;
import java.util.stream.Stream;

public class ReduceExample {

	public static void main(String[] args) {
		Stream<Integer> stream=Stream.of(1,2,3,4,5,6,7,8,9,10);
		//Optional<Integer> optional=stream.reduce((n1,n2)->n1+n2);
		Optional<Integer> optional=stream.reduce((n1,n2)->n1*n2);
		System.out.println(optional.get());
	}

}
