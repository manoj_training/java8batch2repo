import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StringSorting {
	
	public static void main(String[] args) {
		
		List<String> cities=Arrays.asList("indore","ujjain","mumbai","delhi","goa","pune","hyderabad");
		System.out.println(cities);
		//Collections.sort(cities);
		Collections.sort(cities, new StringLengthComparator());
		System.out.println(cities);
	}
	
}
