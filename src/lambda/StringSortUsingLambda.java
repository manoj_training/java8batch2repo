package lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
public class StringSortUsingLambda {
	
	public static void main(String[] args) {
	
		List<String> cities=Arrays.asList("bhopal","agra","chennai","ujjain","goa","rajgarh");

		System.out.println(cities);
		
		Collections.sort(cities, (s1,s2) -> s1.length()-s2.length());
		
		
		/*
		Collections.sort(cities, new Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				return s1.length()-s2.length();
			}
		});
		*/
		System.out.println(cities);
	}
	
	
}
