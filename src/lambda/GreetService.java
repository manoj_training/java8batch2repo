package lambda;

public class GreetService {

	
	public static void sayHello() {
		System.out.println("Hello User...!");
	}
	public static void sayWelcome() {
		System.out.println("Welcome User....!");
	}
	public void sayBye() {
		System.out.println("Bye User....!");
	}
	
	public static void main(String[] args) {
		
		WellWisher.wish(GreetService::sayHello);
		WellWisher.wish(GreetService::sayWelcome);
		WellWisher.wish(new GreetService()::sayBye);
		/*
		Greeting g1=GreetService::sayHello;
		Greeting g2=GreetService::sayWelcome;
		Greeting g3=GreetService::sayBye;
		
		WellWisher.wish(g1);
		WellWisher.wish(g2);
		WellWisher.wish(g3);
		*/
		
		
	}
}
