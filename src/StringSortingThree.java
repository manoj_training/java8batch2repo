import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StringSortingThree {

	public static void main(String[] args) {
	
		List<String> countries= Arrays.asList("india","australia","canada","dubai","america");
		
		System.out.println(countries);
		//Collections.sort(countries);
		
		Collections.sort(countries, new Comparator<String>(){

			@Override
			public int compare(String o1, String o2) {
				return o1.length()-o2.length();
			}
			
		});
		
		
		/*
		Comparator<String> mycomp=new Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				return s2.length()-s1.length();
			}
		};
		*/
		
		//Collections.sort(countries, mycomp);
		
		
		
		
		System.out.println(countries);
		
		

	}

}
